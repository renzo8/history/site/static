# 2021.11.16 TUESDAY

## Highlight

## Todo

## Reminder

| Time | Who | Task | Venue |
| ---- | --- | ---- | ----- |
| 12:45 - 13:45 | Kennc | lunch | Fairwood, Wan Chai |
| 18:00 - 20:00 | Kennc | XXX | Home, Lok Nga Court |
| 20:00 - 20:30 | Kennc | dinner | Home, Lok Nga Court |
| 20:30 - 20:45 | Kennc, Mary, Lorenzo | phone call | Home, Lok Nga Court |
| 20:45 - 21:00 | Kennc | bath | Home, Lok Nga Court |
| 21:00 - 21:30 | Kennc, Lorenzo | xmas preparation (song listening) | Home, Lok Nga Court |
| 21:30 - 22:00 | Kennc, Mary Lorenzo | tv watching (baking impossible) | Home, Lok Nga Court |
| 22:00 - 22:30 | Kennc, Lorenzo | story telling | Home, Lok Nga Court |
| 22:30 - 23:00 | Kennc | tao bao time | Home, Lok Nga Court |
| 23:00 - 23:30 | Kennc | letterland time | Home, Lok Nga Court |