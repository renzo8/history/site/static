## History
| Date | Target | Words |
| ---- | --- | ---- |
| 2021.11.09 TUE U001 | A | { a tent }, { a frog }, { a monkey }, { a man a woman }, { a firefighter } |

## Card List
+ 040 : { an elephant }
+ 237 : { a frog }
+ 247 : { a pig }
+ 279 : { a monkey }
+ 318 : { turtles }
+ 369 : { a man a woman }
+ 376 : { a tent }
+ 405 : { a feather }
+ 429 : { a firefighter }
+ 433 : { tickle }
