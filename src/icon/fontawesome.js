import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faHome, faList, faMapMarkedAlt, faMapSigns, faSchool } from '@fortawesome/free-solid-svg-icons'
library.add(faCoffee)
library.add(faMapSigns)
library.add(faSchool)
library.add(faList)
library.add(faMapMarkedAlt)
library.add(faHome)