import { faFish } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import Calendar from 'react-calendar';
// import 'react-calendar/dist/Calendar.css'; // default calendar style
import '../icon/fontawesome';
import Layout from "./components/common/visual/layout";

const IndexPage = () => {
  
  const [value, onChange] = useState(new Date());

  function showDetails(value) {
    alert(value);
  }
  
  return (
    <Layout pageTitle="">
      <div class="col page-title">TODO</div>
      <div class="d-flex">
        <div class="col-6">
          <Calendar onChange={onChange} value={value} onClickDay={showDetails} />
        </div>
        <div class="col-6">
          <div class="col page-details">
            <h5><FontAwesomeIcon icon={faFish} size="1x" /> Watch [ENGLISH]</h5>
            <ul>
              <li>
                <a target="_blank" rel="noreferrer" href="https://www.in-parents.com/babynews/371/stress">我們該怎樣幫助小孩釋放壓力呢?</a>
              </li>
            </ul>
            {/*
            <h5><FontAwesomeIcon icon={faFish} size="1x" /> Shopping [淘寶]</h5>
            <ul>
              <li>
                <a target="_blank" rel="noreferrer" href="https://wise.com/zh-hk/blog/taobao-hong-kong-guide">淘寶教學</a>
              </li>
            </ul>
            <h5><FontAwesomeIcon icon={faFish} size="1x" /> Class [易經]</h5>
            <ul>
              <li><Link to="/kennc/living/yijin/lesson01/">易經知先天體質及性格 [方法教學]</Link></li>
            </ul>
            */}
            <h5><FontAwesomeIcon icon={faFish} size="1x" /> Watch [ENGLISH]</h5>
            <ul>
              <li>
                <a target="_blank" rel="noreferrer" href="https://www.etchkshop.com/collections/letterland/starter-sets">Letterland - Phonics Introductory Training</a>
              </li>
            </ul>
            <h5><FontAwesomeIcon icon={faFish} size="1x" /> Watch [MUSIC]</h5>
            <ul>
              <li>
                <a target="_blank" rel="noreferrer" href="https://www.hkphil.org/tc/event/what-are-you-playing-1">香港管弦樂團 - 介紹各種樂器</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage