import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";

export default function Fsmodal({ buttonLabel, modalTitle, xlsFilename, children }) {
  const data = useStaticQuery(graphql`
    query {
        allFile {
            edges {
                node {
                    publicURL
                    name
                }
            }
        }
    }`)
    
    return (
        <span>
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">{buttonLabel}</button>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{modalTitle}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">{children}</div>
                    <div class="modal-footer">
                        {
                            data.allFile.edges.map((file) => {
                                if(file.node.name === xlsFilename) {
                                    return (
                                        <a href={file.node.publicURL} download>{file.node.name}</a>
                                    )
                                }
                                return null;
                            })
                        }
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        {/*<button type="button" class="btn btn-primary" onClick={() => { alert('clicked') }}>Download</button>*/}
                    </div>
                </div>
            </div>
            </div>
        </span>
    )
}