import { faDocker } from '@fortawesome/free-brands-svg-icons';
import { faAnchor, faYinYang } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import { Link, navigate } from "gatsby";
import React, { useRef } from 'react';

export default function Header({ siteTitle, pageTitle, children }) {

    const commandValue = useRef('')

    const sendValue = () => {
        let commandline = commandValue.current.value;

        switch(commandline) {
            case 'calc':
                navigate(`/kennc/business/changes/tools/calculator/`);
                break;
            case `diary`:
                navigate(`/kennc/diary/section/`);
                break;
            default:
                alert(commandline);
                break;
        }
    }
    
    return (
        <>
            <div class="d-flex justify-content-between">
                <h1><FontAwesomeIcon icon={faDocker} size="1x" transform={{ rotate:8 }} /> {pageTitle === '' ? siteTitle : pageTitle}</h1>
                <span>
                    <TextField id="command" label="" defaultValue="" size="small" helperText="* testing" inputRef={commandValue} />&nbsp;&nbsp;
                    <Button style={{ backgroundColor: '#000000', color: '#FFFFFF', maxHeight:'25px' }} onClick={sendValue}>run</Button>
                </span>
            </div>
            <div class="d-flex header">
                <ul class="nav">
                    <li class="nav-item nav-link"><Link to="/"><FontAwesomeIcon icon={faAnchor} size="1x" /> home</Link></li>
                    <li class="nav-item nav-link"><Link to="/move/section/"><FontAwesomeIcon icon={faAnchor} size="1x" /> move</Link></li>
                    <li class="nav-item nav-link"><Link to="/kennc/section/"><FontAwesomeIcon icon={faAnchor} size="1x" /> kennc</Link></li>
                    <li class="nav-item nav-link"><Link to="/lorenzo/section/"><FontAwesomeIcon icon={faAnchor} size="1x" /> lorenzo</Link></li>
                    <li class="nav-item nav-link"><Link to="/lorenzo/learning/language/english/"><FontAwesomeIcon icon={faAnchor} size="1x" /> english</Link></li>
                    <li class="nav-item nav-link"><Link to="/kennc/living/yijing/selfstudy/"><FontAwesomeIcon icon={faYinYang} size="1x" /> study</Link></li>
                    {/*
                    <li class="nav-item nav-link"><Link to="/kennc/business/changes/"><FontAwesomeIcon icon={faYinYang} size="1x" /> changes</Link></li>
                    <li class="nav-item nav-link"><Link to="/kennc/business/aow/"><FontAwesomeIcon icon={faBookMedical} size="1x" /> wars</Link></li>
                    <li class="nav-item nav-link"><Link to="/kennc/business/changes/tools/calculator/"><FontAwesomeIcon icon={faCalculator} size="1x" /> calculator</Link></li>
                    */}
                </ul>
            </div>
        </>
    )
}