import * as React from "react";

export default function Footer({ siteAuthor, children }) {
    return (
        <>
            <div class="container-fluid">
                <div class="d-flex flex-column justify-content-center text-center footer">
                    &copy; 2021 { siteAuthor }
                </div>
            </div>
        </>
    )
}