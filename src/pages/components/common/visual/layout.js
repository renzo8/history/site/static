import { graphql, useStaticQuery } from "gatsby";
import * as React from "react";
import { Helmet } from "react-helmet";
import Footer from "./footer";
import Header from "./header";

export default function Layout({ pageTitle, children }) {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          author
          url
        }
      }
    }
  `)

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>{data.site.siteMetadata.title}</title>
        <base href={data.site.siteMetadata.url}></base>
      </Helmet>
      <div className="flex-div">
        <Header siteTitle={data.site.siteMetadata.title} pageTitle={pageTitle}></Header>
        <div className="flex-content">{children}</div>
        <Footer siteAuthor={data.site.siteMetadata.author}></Footer>
      </div>
    </>
  )
}