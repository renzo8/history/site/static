import * as React from "react";

const JobPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3>COMMERCIAL</h3>
        <ul>
          <li><a target="_blank" rel="noreferrer" href="https://uk.indeed.com/">indeed</a></li>
          <li><a target="_blank" rel="noreferrer" href="https://www.glassdoor.co.uk/">glassdoor</a></li>
        </ul>
        <h3>GOVERNMENT</h3>
        <ul>
          <li><a target="_blank" rel="noreferrer" href="https://www.jobcentreguide.co.uk/">job centre plus</a></li>
        </ul>
        <h3>AGENT</h3>
        <ul>
          <li>xxx</li>
        </ul>
      </div>
    </div>
  )
}

export default JobPage
