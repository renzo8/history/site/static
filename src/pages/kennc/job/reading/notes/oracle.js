import * as React from "react";

const OraclePage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3><a target="_blank" rel="noopener noreferrer" href="https://www.oracle.com/index.html">ORACLE</a></h3>

        <p>Oracle Database Version</p>
        <ul>
            <li>12c</li>
            <li>19c</li>
        </ul>

        <p>Oracle Enterprise Manager Cloud Control (OEM / EMCC)</p>
        <ul>
            <li>12c</li>
            <li>13c</li>
        </ul>

        <p>DBA</p>
        <ul>
          <li>Installation [Windows, Linux]</li>
          <li>Maintenance [Patch, Upgrade, Recovery, Backup]</li>
          <li>Monitor - Reports [AWR, ASH, ADDM]</li>
          <li>Tuning</li>
        </ul>

        <p>Reference</p>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.support.dbagenesis.com">DBA Genesis Support</a></li>
        </ul>
      </div>
    </div>
  )
}

export default OraclePage