import * as React from "react";

const OsOracleLinuxPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3>LINUX (ORACLE)</h3>

        <p>Reference</p>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.support.dbagenesis.com/post/oracle-12c-installation-on-oracle-linux">Oracle 12c Installation on Oracle Linux 7</a></li>
        </ul>
      </div>
    </div>
  )
}

export default OsOracleLinuxPage
