import * as React from "react";

const NginxPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3><a target="_blank" rel="noopener noreferrer" href="https://www.nginx.com/">NGINX</a></h3>
        <p>Setup</p>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://hub.docker.com/_/nginx">docker pull nginx</a></li>
        </ul>
        <p>Resources</p>
        <ul>
          <li>$src/data/docker/nginx/config</li>
          <li>$src/data/docker/nginx/public</li>
          <li>$src/data/docker/nginx/run</li>
        </ul>
        <p>Operations</p>
        <ul>
          <li>START: $src/data/docker/nginx/run/start.cmd</li>
          <li>END: $src/data/docker/nginx/run/end.cmd</li>
        </ul>
        <p>Features</p>
        <ol>
          <li>Hosting</li>
          <li>Reverse Proxy</li>
          <li>SSL</li>
          <li>Load Balance</li>
        </ol>
        <p>Configurations</p>
        <ul>
          <li>xxx</li>
          <li>yyy</li>
          <li>zzz</li>
        </ul>
        <p>References</p>
        <ul>
          <li>https://linyencheng.github.io/2019/07/13/tool-nginx/</li>
        </ul>
      </div>
    </div>
  )
}

export default NginxPage
