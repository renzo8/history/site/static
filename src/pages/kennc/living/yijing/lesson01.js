import { faYinYang } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as React from "react";

const YijinLesson01Page = () => {
  return (
    <>
      <div class="container-fluid">
        <div class="row">
          <h3><FontAwesomeIcon icon={faYinYang} size="1x" spin /> 易經 . 極速易經體驗班</h3>
          <p>
            時間: 19:30 - 22:00<br/>
            地點: 尖沙咀星光行1728室<br/>
            講員: 范師母
          </p>
          <h5>易經知先天體質及性格 [方法教學]</h5>
          <ul>
            <li>計算屬於自己的先天體質卦象</li>
            <li>知道如何保養及了解自己的潛在性格</li>
          </ul>
        </div>
      </div>
    </>
  )
}

export default YijinLesson01Page
