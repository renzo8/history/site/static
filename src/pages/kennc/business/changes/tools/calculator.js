import { faPagelines } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import React from "react";
import { v4 as uuidv4 } from 'uuid';
import dummyData from "../../../../../data/json/dummy.json";

const CalculatorPage = () => {
  const symbolList = ['', '☰', '☱', '☲','☳','☴','☵','☶','☷'];
  // const wordList = ['', '乾','兌','離','震','巽','坎','艮','坤'];
  const baseTotal = 8;
  const cardTotal = baseTotal * baseTotal;
  // const matrixTotal = baseTotal * 2;
  const skyTotal = 10;
  // const landTotal = 12;

  // function universeMatrix() {
  // }

  function getBias() {
    return [4, 5, 6, 7];
  }
  
  function getRandomUuidList() {
    let uuidList = [];
    for(let i=1; i<=cardTotal; i++) {
      uuidList.push(uuidv4().toLowerCase().replaceAll("-", ""));
    }
    return uuidList;
  }

  function getUuid() {
    let uuid = 0;
    let uuidList = getRandomUuidList();
    let biasArray = getBias();

    uuid = pickUuid(uuidList, biasArray);
    return uuid;
  }
  
  function pickUuid(uuidList, biasArray) {
    let pickupArray = uuidList;
    for(let i=0; i<biasArray.length; i++) {
      let loopCount = biasArray[i];
      for(let j=0; j<loopCount; j++) {
        pickupArray = shuffle(pickupArray);
      }
    }

    return uuidList[0];
  }

  function calculateUuid(uuid) {
    let total = 0;
    for (let n of uuid) {
      let tempNum = n;
      if(isNaN(n)) {
        tempNum = getRandomNumber();
      } 
      
      if(parseInt(n) === 0) {
        tempNum = getRandomNumber();
      }
      
      total += parseInt(tempNum);
    }

    return checkNumber(total);
  }

  function checkNumber(total) {
    if(total.toString().length === 1) {
      return tuneNumber(total);
    }

    return calculateUuid(total.toString());
  }
  
  function tuneNumber(number) {
    return number > baseTotal ? (number - baseTotal) : number;
  }

  function getRandomNumber() {
    return Math.floor(Math.random() * skyTotal) + 1;
  }

  function getChangeValue() {
    let current = new Date();
    
    let hour = current.getHours();
    let minute = current.getMinutes();
    let second = current.getSeconds();
    
    return hour + ':' +  minute + ':' + second;
  }

  function getSymbol(id) {
    return symbolList[id];
  }

  // function getWord(id) {
  //  return wordList[id];
  // }

  function shuffle(array) {
    let i = array.length;
    while (i !== 0) {
      let r = Math.floor(Math.random() * i);
      i--;
      [array[i], array[r]] = [array[r], array[i]];
    }
    return array;
  }

  let timeValue = ``
  let skyCard = ``;
  let landCard = ``;
  let shuffledCards = [];
  let shuffledPositions = [];
  let isShuffled = false;

  function shuffleCard() {
    let skyShuffleCardNumber = 0;
    let landShuffleCardNumber = 0;
    let manShufflePositionNumber = calculateUuid(getUuid()); // TODO: Replace by BIAS

    // Shuffle Position
    let positionList = [11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44];
    skyShuffleCardNumber = calculateUuid(getUuid());
    landShuffleCardNumber = calculateUuid(getUuid());
    positionList = shuffleArray(positionList, skyShuffleCardNumber * landShuffleCardNumber);
    positionList = shuffleArray(positionList, manShufflePositionNumber);
    skyShuffleCardNumber = 0;
    landShuffleCardNumber = 0;
    shuffledPositions = positionList;

    // Shuffle Sky Card Set
    let skyCardList = [1,2,3,4,5,6,7,8];
    skyShuffleCardNumber = calculateUuid(getUuid());
    skyCardList = shuffleArray(skyCardList, skyShuffleCardNumber);
    skyShuffleCardNumber = 0;

    // Shuffle Land Card Set
    let landCardList = [1,2,3,4,5,6,7,8];
    landShuffleCardNumber = calculateUuid(getUuid());
    landCardList = shuffleArray(landCardList, landShuffleCardNumber);
    landShuffleCardNumber = 0;

    // Shuffle Man Card Set
    let manCardList = [];
    manCardList = manCardList.concat(skyCardList, landCardList);
    skyShuffleCardNumber = calculateUuid(getUuid());
    landShuffleCardNumber = calculateUuid(getUuid());
    manCardList = shuffleArray(manCardList, skyShuffleCardNumber * landShuffleCardNumber);
    manCardList = shuffleArray(manCardList, manShufflePositionNumber);
    skyShuffleCardNumber = 0;
    landShuffleCardNumber = 0;
    shuffledCards = manCardList;
  }

  function shuffleArray(sourceList, counter) {
    let shuffledList = sourceList;
    for(let i=1; i<=counter; i++) {
      shuffle(sourceList);
    }
    
    return shuffledList;
  }

  function assignCard(randomPosition) {
    let randomNumber = parseInt(randomPosition.replace(`card`,``));
    let positionNumber = shuffledPositions.findIndex(n => n === randomNumber);
    return shuffledCards[positionNumber];
  }
  
  function pickCard(cardId) {
    if(!isShuffled) {
      isShuffled = true;
      shuffleCard();
      createBoardReference();
    }

    let cardObject = document.getElementById(cardId);

    if(!skyCard) {
      timeValue = getChangeValue();
      skyCard = assignCard(cardId);
      cardObject.style.backgroundColor = `#FFFFFF`;
      cardObject.innerHTML = getCardSymbol(skyCard);
      return;
    }

    if(!landCard) {
      landCard = assignCard(cardId);
      cardObject.style.backgroundColor = `#000000`;
      cardObject.innerHTML = getCardSymbol(landCard);
      createReport();
      return;
    }

    alert(`PLEASE ASK ANOTHER QUESTION`);
  }

  function getCardSymbol(cardNumber) {
    return `<span class="chosen">` + getSymbol(cardNumber) + `</span>`;
  }

  //function getColorSignal(skyNumber, landNumber) {
  //  let agreeColor = `#FF0000`;
   // let disagreeColor = `#0000FF`;
   // let passColor = `#000000`;
   // return agreeColor;
 // }
  
  function createReport() {
    let reportObject = document.getElementById(`insight`);
    let displayDrawing = `<p>` + timeValue + `: ` + getSymbol(skyCard) + "" + getSymbol(landCard) + ` <p>`;
    let displayFinding = `<p>` + constructResult(getSymbol(skyCard), getSymbol(landCard)) + `<p>`;
    let reportDetails = [displayDrawing, displayFinding];
    reportObject.innerHTML = reportDetails.join(`<hr/>`);
  }
  
  function createBoardReference() {
    let boardReferenceObject = document.getElementById(`board`);
    let referenceDetails = ``;
    dummyData.board.map((data, index) => {
      let row = data.symbols;
      row.map((cell, id) => {
        referenceDetails += `<li>` + constructReference(id + 1, cell)+ `</li>`;
      });
    });
    boardReferenceObject.innerHTML = `<ul>` + referenceDetails + `</ul>`;
  }

  function constructResult(skySymbol, landSymbol) {
    let result = ``;
    dummyData.board.map((data, index) => {
      let row = data.symbols;
      row.map((cell, id) => {
        if(skySymbol === cell.sky && landSymbol === cell.land) {
          console.log(skySymbol + ` vs ` + cell.sky);
          console.log(landSymbol + ` vs ` + cell.land);
          console.log(constructReference(id + 1, cell));
          result = constructReference(id + 1, cell);
        }
      });
    });
    return result === `` ? `retry` : result;
  }

  function constructReference(boardRowNumber, symbolNode) {
    return `<span style="background-color:#`+ symbolNode.color + ` !important" class="badge bg-secondary">`
      + symbolNode.yid  + `: ` + symbolNode.name 
      + ` (` + symbolNode.attribute + `)` 
      + ` [` + symbolNode.sky + symbolNode.land + `]`
      +  `</span>`;
  }

  function reloadPage() {
    window.location.reload();
  }

  return (
    <div class="container-fluid waterbackground">
      <div class="row">
        <h3>CALCULATOR</h3>
        <p>Bias Panel</p>
        <p>☲<TextField id="question" label="" defaultValue="" size="small" helperText="* type what is going to ask"/>☰</p>
        <p>
          <Button style={{ backgroundColor: '#FF0000', maxHeight:'25px' }} onClick={reloadPage}>
            <span class="skyColor">☲</span>
            <span class="landColor">☰</span>
          </Button>
        </p>
        <p>
          <table class="table cardpool">
            {/*
            <thead>
              <tr>
                <th scope="col" colspan="4" class="text-center">
                  <p>
                    <Button style={{ backgroundColor: '#000000', color: '#FFFFFF', maxHeight:'25px' }} onClick={shuffleCard}>shuffle</Button>
                  </p>
                </th>
              </tr>
            </thead>
            */}
            <tbody>
              <tr>
                <td><div id="card14" onClick={() => pickCard(`card14`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card13" onClick={() => pickCard(`card13`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card12" onClick={() => pickCard(`card12`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card11" onClick={() => pickCard(`card11`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
              </tr>
              <tr>
                <td><div id="card24" onClick={() => pickCard(`card24`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card23" onClick={() => pickCard(`card23`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card22" onClick={() => pickCard(`card22`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card21" onClick={() => pickCard(`card21`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
              </tr>
              <tr>
                <td><div id="card34" onClick={() => pickCard(`card34`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card33" onClick={() => pickCard(`card33`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card32" onClick={() => pickCard(`card32`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card31" onClick={() => pickCard(`card31`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
              </tr>
              <tr>
                <td><div id="card44" onClick={() => pickCard(`card44`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card43" onClick={() => pickCard(`card43`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card42" onClick={() => pickCard(`card42`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
                <td><div id="card41" onClick={() => pickCard(`card41`)} class="yijingcard d-flex align-items-center justify-content-center"><FontAwesomeIcon icon={faPagelines} size="2x" /></div></td>
              </tr>
            </tbody>
          </table>
        </p>
        <h3>RESULT</h3>
        <hr/>
        <div id="insight"></div>
        <hr/>
        <div class="displayReference">
          <p>時辰</p>
          <ul>
            <li>23:00-01:00 / 子時 / 1</li>
            <li>01:00-03:00 / 丑時 / 2</li>
            <li>03:00-05:00 / 寅時 / 3</li>
            <li>05:00-07:00 / 卯時 / 4</li>
            <li>07:00-09:00 / 辰時 / 5</li>
            <li>09:00-11:00 / 巳時 / 6</li>
            <li>11:00-13:00 / 午時 / 7</li>
            <li>13:00-15:00 / 未時 / 8</li>
            <li>15:00-17:00 / 申時 / 9</li>
            <li>17:00-19:00 / 酉時 / 10</li>
            <li>19:00-21:00 / 戌時 / 11</li>
            <li>21:00-23:00 / 亥時 / 12</li>
          </ul>
          <p>卦象</p>
          <div id="board"></div>
        </div>
      </div>
    </div>
  )
}

export default CalculatorPage
