import { faBookOpen, faChessKing, faChessKnight, faGamepad, faToolbox, faUniversity } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as React from "react";
import sourceGraph01 from '../../../diagram/changes/1.gif';
import sourceGraph02 from '../../../diagram/changes/2.gif';
import sourceGraph03 from '../../../diagram/changes/3.gif';
import sourceGraph04 from '../../../diagram/changes/4.gif';
import sourceGraph05 from '../../../diagram/changes/5.gif';
import sourceGraph06 from '../../../diagram/changes/6.gif';
import sourceGraph07 from '../../../diagram/changes/7.gif';
import sourceGraph08 from '../../../diagram/changes/8.gif';
import sourceGraph09 from '../../../diagram/changes/9.gif';
import afterGraph from '../../../diagram/changes/after.gif';
import beforeGraph from '../../../diagram/changes/before.gif';
import directionAfterGraph from '../../../diagram/changes/directionafter.gif';
import directionBeforeGraph from '../../../diagram/changes/directionbefore.gif';
import historyMilestoneGraph from '../../../diagram/changes/historymilestone.gif';
import sundegreeGraph from '../../../diagram/changes/sundegree.gif';
import yeargasGraph from '../../../diagram/changes/yeargas.gif';

const ChangesPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3><FontAwesomeIcon icon={faBookOpen} size="1x" /> 簡介</h3>
        <ul>
          <li>
              <p>河圖, 背部負有圖紋的龍馬出現在黃河地區 (伏羲氏時代)</p>
              河圖是1到5相加而成的數列
              <p>洛書, 背面負有圖紋的神龜出現在洛水 (夏禹治水時)</p>
              洛書是由1到9排列成縱、橫、斜各方向的數字和都是15的數學魔陣，這是世界上最早的數學魔陣。
              <p>參考</p>
              http://web2.nmns.edu.tw/PubLib/NewsLetter/88/135/13.html
          </li>
          <li>天地平衡</li>
          <li>日月</li>
          <li>宇宙 = 棋盤, 周易時間空間體系 = 利用 周髀算經法則 復原 宇宙模式</li>
          <li>取向: 天地之於人</li>
          <li>意識化萬物, 外在型是手段非目的 > 卦象, 納音</li>
          <li>術數根本: 河圖洛書, 先天八卦, 後天八卦</li>
          <li>上古三大奇書: 易經, 山海經, 皇帝內經 > 三大奇書的順序是天、地、人</li>
          <li>自古醫卜是一家，一個看實病，一個看虛病，一陰一陽，醫‘道’之不足。</li>
          <li>todo: https://kknews.cc/culture/kajmqnb.html</li>
          <li>search: 然後代入，以陽陰一組分成五組的十天干，於先天八卦(體)運行的「三次」模式之中(留意上表內紅字位置變化)：</li>
          <li>高島吞象: 高島嘉右衛門占例集, 增補高島占卜, 高島易斷</li>
        </ul>
        
        <h3><FontAwesomeIcon icon={faUniversity} size="1x" /> 學習</h3>
        <p>河圖</p>
        <ul>
          <li>生數, 始生之數, 代表陰, [1 , 2, 3, 4, 5]</li>
          <li>成數, 由生數累積而成, 代表陽 [6, 7, 8, 9, 10]</li>
          <li>陰主內, 陽主外</li>
          <li>一奇(單數)一偶(雙數), 代表一陰一陽, 表示陰陽合德</li>
          <li>天數, [1, 3, 5, 7, 9], += 25</li>
          <li>地數, [2, 4, 6, 8, 10], += 30</li>
          <li>河圖的天地數, 25 + 30 = 55</li>
        </ul>
        
        <p>河圖(八卦圖) > 洛書(九宮圖)</p>
        <ul>
          <li>把2和8的位置對調 > 生成數學魔陣: 15</li>
          <li>紅點代表奇數(陽), 藍點代表偶數(陰), 將魔陣還原加連線 > 洛書</li>
          <li>奇數(陽) 居正, 偶數(陰) 居負</li>
          <li>若中五不用，下上、左右、對角相加都等於10, 代表十全</li>
          <li>天數, [1, 3, 5, 7, 9] += 25</li>
          <li>地數, [2, 4, 6, 8] += 20</li>
          <li>洛書的天地數, 25 + 20 = 45</li>
        </ul>

        <p>先天八卦 (伏羲八卦次序 / 伏羲八卦方位圖)</p>
        <ul>
          <li>次序: [乾, 兌, 離, 震, 巽, 坎, 艮, 坤]</li>
          <li><img src={beforeGraph} alt="before" /></li>
          <li><img src={directionBeforeGraph} alt="after" /></li>
        </ul>

        <p>後天八卦 (文王八卦次序 / 文王八卦方位圖)</p>
        <ul>
          <li>次序: [乾, 坎, 艮, 震, 巽, 離, 坤, 兌]</li>
          <li>後天八卦方位與東南西北方位存在著一一對應的關係</li>
          <li><img src={afterGraph} alt="after" /></li>
          <li><img src={directionAfterGraph} alt="after" /></li>
        </ul>

        <p>五行</p>
        <ul>
          <li>五行家把1到10的數字分屬五行</li>
          <li>叫一六水、二七火、三八木、四九金、五十土，並唸做「天一生水，地六成之；地二生火，天七成之；天三生木，地八成之；地四生金，天九成之；天五生土，地十成之」。</li>
          <li>因水居北、火居南、木居東、金居西、土居中，故又變成北水、南火、東木、西金、中土。</li>
        </ul>

        <p>納音五行圖 (黃帝內經)</p>
        <ul>
          <li>天地一體、四時一體、六氣一體、萬物一體、五臟一體、成敗倚伏生乎動、人與天地相應。</li>
          <li>人有男女、天地有陰陽，人有五臟六腑、天地有五運六氣</li>
          <li>五運是天干所化之象，也就是五行</li>
          <li>六氣是地支所化之象，就是六種表象</li>
          <li>五運配六氣爲三十象。三十象配陰陽爲六十花甲子。這就是《納音五行圖》的根。</li>
          <li>todo: https://www.xuehua.us/a/5ebbf53a86ec4d140ff1a3c7?lang=zh-tw</li>
          <li></li>
          <li></li>
          <li></li>
        </ul>

        <p>甲子六十 (還曆 / 還甲 / 回甲 / 花甲)</p>
        <ul>
          <li>計算年齡時，都以天干地支的順序來表示</li>
          <li>一個循環從甲子開始, 接著是乙丑、丙寅... 依序共有六十個組合，干支每六十年就會循環一次</li>
          <li>人的年齡滿六十虛歲，也說六十甲子，即一個人到60歲（出生時1虛歲）時，就逢一甲子，又稱一輪，正是新一輪重新算起的時候</li>
          <li>干為主幹，支為分支, 在商朝開始出現天干與地支配合用以紀日，使用干支紀年要更晚一些。</li>
          <li><p>干: 循環, [植物的生長, 人體部位計數、排序]</p></li>
          <li>
            <p>天干, [甲, 乙, 丙, 丁, 戊, 己, 庚, 辛, 壬, 癸], 十進位</p>
            <p>天干符號按其含義與五行符號和五方進行匹配</p>
            <table class="table table-dark">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">木</th>
                  <th scope="col">火</th>
                  <th scope="col">土</th>
                  <th scope="col">金</th>
                  <th scope="col">水</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>(方向)</td>
                  <td>(東)</td>
                  <td>(南)</td>
                  <td>(中)</td>
                  <td>(西)</td>
                  <td>(北)</td>
                </tr>
                <tr>
                  <td>陽</td>
                  <td>甲 / 4</td>
                  <td>丙 / 6</td>
                  <td>戊 / 8</td>
                  <td>庚 / 0</td>
                  <td>壬 / 2</td>
                </tr>
                <tr>
                  <td>陰</td>
                  <td>乙 / 5</td>
                  <td>丁 / 7</td>
                  <td>己 / 9</td>
                  <td>辛 / 1</td>
                  <td>癸 / 3</td>
                </tr>
              </tbody>
            </table>
            <p>??? * /n = 年份除12餘數</p>
            <p>??? 天干相沖：甲庚相沖(陽東西)　辛乙相沖(陰東西)　丙壬相沖(陽南北)　丁癸相沖(陰南北)</p>
            <p>??? 天干相剋：甲乙木剋戊己土　丙丁火剋庚辛金　戊己土剋壬癸水　庚辛金剋甲乙木　壬癸水剋丙丁火</p>
            <p>??? 天干化合：甲己合化土　乙庚合化金　丙辛合化水　丁壬合化木　戊癸合化火</p>
          </li>
          <li>
            <p>地支, [子, 丑, 寅, 卯, 辰, 巳, 午, 未, 申, 酉, 戌, 亥], 十二進位</p>
            <table class="table table-dark">
              <thead>
                <tr>					
                  <th scope="col">#</th>
                  <th scope="col">水</th>
                  <th scope="col">濕土</th>
                  <th scope="col">木</th>
                  <th scope="col">火</th>
                  <th scope="col">燥土</th>
                  <th scope="col">金</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>陽</td>
                  <td>子</td>
                  <td>辰</td>
                  <td>寅</td>
                  <td>午</td>
                  <td>戌</td>
                  <td>申</td>
                </tr>
                <tr>
                  <td>陰</td>
                  <td>亥</td>
                  <td>丑</td>
                  <td>卯</td>
                  <td>巳</td>
                  <td>未</td>
                  <td>酉</td>
                </tr>
              </tbody>
            </table>
          </li>
          <li>
            <p>時辰</p>
            <ul>
              <li>23:00-01:00 / 子時 / 1</li>
              <li>01:00-03:00 / 丑時 / 2</li>
              <li>03:00-05:00 / 寅時 / 3</li>
              <li>05:00-07:00 / 卯時 / 4</li>
              <li>07:00-09:00 / 辰時 / 5</li>
              <li>09:00-11:00 / 巳時 / 6</li>
              <li>11:00-13:00 / 午時 / 7</li>
              <li>13:00-15:00 / 未時 / 8</li>
              <li>15:00-17:00 / 申時 / 9</li>
              <li>17:00-19:00 / 酉時 / 10</li>
              <li>19:00-21:00 / 戌時 / 11</li>
              <li>21:00-23:00 / 亥時 / 12</li>
            </ul>
          </li>
          <li>
            <p>陰曆</p>
            <ul>
              <li>古代曆法</li>
              <li>按月亮的月相周期來安排的曆法</li>
              <li>望月 (滿月光), 地球處於月亮與太陽之間時，月亮被太陽照亮的半球朝向地球</li>
              <li>朔月 (新月), 月亮處於太陽和地球之間，黑暗半球對著我們，導致幾乎無法看到月亮。</li>
              <li>朔日 (新月), 月亮和太陽幾乎同時從東方升起，即使地球把太陽光反射到月亮，然後再由月亮反射回來的那部分光（稱為地照光），也會完全被太陽的強光淹沒</li>
              <li>月相變化的周期叫做朔望月, 月亮繞地球運行一周時間為一個月 (從朔到下一個朔或從望到下一個望的時間 )</li>
              <li>朔望月的長度並不固定，最長可達29天19小時多，最短為29天6小時4分多，平均長度約為29天12小時44分3秒。</li>
              <li>與地球漲潮落潮有關，與航海、捕魚也有密切的關係。</li>
              <li>* 記時單位</li>
              <li>夏曆以「朔」定義每月初一</li>
            </ul>
            <p>陽曆 / 公曆 / 西曆</p>
            <ul>
              <li>起源於6000多年前的古埃及</li>
              <li>據地球圍繞太陽公轉軌道位置，或地球上所呈現出太陽直射點的週期性變化，所制定的曆法；</li>
              <li>不據月亮的月相周期，歲實為365.2421897日，</li>
              <li>有大小月之分，一、三、五、七、八、十、十二月各三十一日；</li>
              <li>四、六、九、十一月各三十日。而二月平年二十八日，閏年二十九日。</li>
              <li>回歸陽曆, 太陽直射點週期性變化 (日照時間), 定為一個回歸年，所制定的曆法</li>
              <li>恆星陽曆, 太陽與黃道帶星群存在一個週期變化 (星群位置), 定為一個恆星年，所制定的曆法</li>
            </ul>

            <p>農曆 / 陰陽曆</p>
            <ul>
              <li>單個陰曆或陽曆無法同時滿足年和月的準確週期。</li>
              <li>利用天文學觀察兼顧 月相(由地球上所觀看之月光形態)週期 和 太陽週期 運動所安排, 且一年的月數必須是整數 (同時考慮到太陽及月球運動)</li>
              <li>採用陰陽合曆的主要目的是配合季節</li>
              <li>陰陽曆:「陽」是地球環繞太陽公轉，以冬至迴歸年為基準確定歲實，配合季節陽光分一歲為二十四節氣；「陰」根據月球運行定朔望月。</li>
              <li>華夏以農立國，農民常依此曆進行農事，故又稱為農民曆</li>
              <li>由太陽之高度角變化週期，影響地表氣候環境之不同，定出二十四節氣</li>
              <li>年、月表徵地球公轉</li>
              <li>日、時表徵地球自轉</li>
            </ul>
          </li>
          <li>
            <p>生辰八字 (八字)</p>
            <ul>
              <li>使用天干、地支組合來表達年、月、日、時辰</li>
              <li>柱 = 1干 + 1支組合</li>
              <li>年柱 = 「年干，年支」</li>
              <li>月柱 = 「月干，月支」</li>
              <li>日柱 = 「日干，日支」</li>
              <li>時柱 = 「時干，時支」</li>
              <li>四柱, 「年柱」、「月柱」、「日柱」、「時柱」</li>
              <li>八字, 4個干字 + 4個支字組成生辰紀日</li>
              <li>四柱 = 八字</li>
              <li>八字命理, 夏朝 > 宋朝徐子平《淵海子平》, 紀念徐子平對八字命理學的貢獻: 子平八字學, 相傳 / 歷史上未必有徐子平此人</li>
              <li>每一年以農曆立春為新一年的開始</li>
            </ul>
          </li>
          <li>
            <p>二十四節氣</p>
            <ul>
              <li><a target="_blank" rel="noopener noreferrer" href="https://zh.wikipedia.org/wiki/%E8%8A%82%E6%B0%94">wiki</a></li>
              <li><a target="_blank" rel="noopener noreferrer" href="http://www.hko.gov.hk/tc/gts/time/24solarterms.htm">天文台</a></li>
              <li>一年 > 2個半年 [冬至, 夏至]</li>
              <li>4氣(2陰2陽): 一年 > 4個季 > (少陽) [冬至, 春分], (太陽) [春分, 夏至], (少陰) [夏至, 秋分], (太陰) [秋分, 冬至]</li>
              <li>6氣(3陰3陽): 一年 > 8個節 > (少陽) [冬至, 雨水, 春分], (太陽) [春分, 穀雨, 夏至], (少陰) [夏至, 處暑, 秋分], (太陰) [秋分, 霜降, 冬至]</li>
              <li>12氣(6陰6陽): 一年 > 12個節 > (少陽) [], (太陽) [], (少陰) [], (太陰) []</li>
              <li>冬至: 陰極陽來</li>
              <li>夏至: 陽極陰來</li>
            </ul>
          </li>
        </ul>

        <p>納音五行</p>
        <ul>
          <li>音: 聲音 / 消息, 信息 (自然界的一切現象都是消息) / ...</li>
          <li>納音 x 五行: 術數理論架構</li>
          <li>接納消息歸屬五行是真正的解釋</li>
          <li></li>
        </ul>

        <h3><FontAwesomeIcon icon={faChessKing} size="1x" /> 思路</h3>
        <ul>
          <li>真有其人</li>
          <li>真有其書</li>
          <li>豈有此理</li>
          <li>真有此理</li>
        </ul>

        <h3><FontAwesomeIcon icon={faChessKnight} size="1x" /> 陷陣</h3>
        <ul>
          <li>河圖洛書 (五行水位) > 水造的人</li>
          <li>山南水北 (五行水位) > 中國位於北半球，以我們的視點來觀察太陽會是從東方升起經由偏南方最後落到西方，山的南面是向陽坡，山的北面是背光坡，南面的日照一定較北面充足，所以山南謂陽、山北謂陰。</li>
          <li>山南水北 (五行水位) > 中國地形是西北高，向東南漸低，河流在流動時會傾向於往東南方向流動，南岸較容易受到河水的侵蝕，形成南濕北乾的情形，故稱水北為陽、水南為陰。</li>
          <li>
            <p>陽宅標準: 山坡的南面、河流的北側</p>
            光照充足的陽處生活, 座北朝南的房子(北面的山峰阻擋北來的冷風侵襲，可以保證氣候上的穩定), 南面臨水 (保證適宜的濕度和開闊的視野，自然使居者保持一個舒適的居住環境)
          </li>
          <li>細探先天八卦圖（伏羲八卦圖） > https://www.eee-learning.com/article/3845</li>
          <li>在易經上天數指奇數, 地數指偶數</li>
          <li>天地數 = 河圖天地數 + 洛書天地數 = 55 + 45 = 100 = 10 x 10</li>
          <li>天地圖 (魔方): 10 x 10方陣, 一半代表河圖 一半代表洛書</li>
          <li>天地圖 (變形): 三角形, 河圖, 洛書的天地數交合在一起</li>
          <li>干支組合 (60年): 天干循環(10) 和 地支循環(12) 的最小公倍數</li>
          <li>天干: 大循環系統 (年計)</li>
          <li>地支: 小循環系統 (月計, 日計, 時計)</li>
          <li>卦: 易經工具, 敍述法則及意義</li>
          <li><a target="_blank" rel="noopener noreferrer" href="https://kknews.cc/culture/kajmqnb.html">易經圖典 (漫畫)</a></li>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.books.com.tw/products/0010642227">圖解易經</a></li>
          <li>
            <p><a target="_blank" rel="noopener noreferrer" href="https://zh.wikipedia.org/wiki/%E5%91%A8%E9%AB%80%E7%AE%97%E7%B6%93">周髀算經</a></p>
            <ul>
              <li>先定冬至(日影最長), 夏至(日影最短)</li>
            </ul>
          </li>
          <li>智慧樹: 理解奧秘</li>
          <li>氣: 由血帶氣</li>
          <li>風水: 設計氣聚的類型 > (氣成質成數)</li>
          <li>10天干和12地支各有其五行, 相配後要表達這個「六十進制」中的一個單元, 需要使用納音五行才能夠「單一」表達</li>
        </ul>
        
        <h3><FontAwesomeIcon icon={faGamepad} size="1x" /> 應用</h3>
        <ul>
          <li>性格</li>
          <li>健康</li>
          <li>電話</li>
          <li>門牌</li>
          <li>風水</li>
        </ul>

        <h3><FontAwesomeIcon icon={faToolbox} size="1x" /> 小工具</h3>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://tw.xingbar.com/calendar/wannianli.html">萬年曆</a></li>
          <li><img src={sourceGraph01} alt="1" /></li>
          <li><img src={sourceGraph02} alt="2" /></li>
          <li><img src={sourceGraph03} alt="3" /></li>
          <li><img src={sourceGraph04} alt="4" /></li>
          <li><img src={sourceGraph05} alt="5" /></li>
          <li><img src={sourceGraph06} alt="6" /></li>
          <li><img src={sourceGraph07} alt="7" /></li>
          <li><img src={sourceGraph08} alt="8" /></li>
          <li><img src={sourceGraph09} alt="9" /></li>
          <li><img src={historyMilestoneGraph} alt="milestone" /></li>
          <li>
            <p>坤 ☷, 艮 ☶, 坎 ☵, 巽 ☴, 震 ☳, 離 ☲, 兌 ☱, 乾 ☰</p>
            <p></p>
            <table class="table table-dark">
              <thead>
                <tr>					
                  <th scope="col">#</th>
                  <th scope="col">2進制</th>
                  <th scope="col">八卦</th>
                  <th scope="col">卦象</th>
                  <th scope="col">兩儀</th>
                  <th scope="col">四象</th>
                  <th scope="col">五行</th>
                  <th scope="col">先天八卦方位</th>
                  <th scope="col">後天八卦方位</th>
                </tr>
              </thead>
              <tbody>
                <tr><td>1</td><td>111</td><td>乾</td><td>☰</td><td>陽</td><td>太陽</td><td>金</td><td>  南</td><td>西北</td></tr>
                <tr><td>2</td><td>110</td><td>兌</td><td>☱</td><td>陽</td><td>太陽</td><td>金</td><td>東南</td><td>  西</td></tr>
                <tr><td>3</td><td>101</td><td>離</td><td>☲</td><td>陽</td><td>少陰</td><td>火</td><td>  東</td><td>  南</td></tr>
                <tr><td>4</td><td>100</td><td>震</td><td>☳</td><td>陽</td><td>少陰</td><td>木</td><td>東北</td><td>  東</td></tr>
                <tr><td>5</td><td>011</td><td>巽</td><td>☴</td><td>陰</td><td>少陽</td><td>木</td><td>西南</td><td>東南</td></tr>
                <tr><td>6</td><td>010</td><td>坎</td><td>☵</td><td>陰</td><td>少陽</td><td>水</td><td>  西</td><td>  北</td></tr>
                <tr><td>7</td><td>001</td><td>艮</td><td>☶</td><td>陰</td><td>太陰</td><td>土</td><td>西北</td><td>東北</td></tr>
                <tr><td>8</td><td>000</td><td>坤</td><td>☷</td><td>陰</td><td>太陰</td><td>土</td><td>  北</td><td>西南</td></tr>
              </tbody>
            </table>
          </li>
          <li><img src={yeargasGraph} alt="year-gas" /></li>
          <li><img src={sundegreeGraph} alt="sun-degree" /></li>
          <li>來年運程 = YYYY - 60</li>
          <li>英 > 數</li>
        </ul>

      </div>
    </div>
  )
}

export default ChangesPage
