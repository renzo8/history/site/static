import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import CryptoJS from 'crypto-js';
import { graphql } from "gatsby";
import React, { useRef, useState } from "react";

const DiarySectionPage = ({ data }) => {
  const { blogkey } = data.site.siteMetadata
  const hexkey = getHexKey(blogkey);
  const text = `迷迭香的香味能讓人提振情緒、對抗憂鬱，平衡緊繃的情緒，讓人打開心胸。能作為麻醉鎮定劑、殺菌、緩解痙攣、收斂劑，也可以用為提振精神、促進血液循環。對橘皮組織、蜂窩組織、頭皮屑、掉髮、記憶力問題、頭痛、肌肉痛等問題有幫助。用在護髮產品中，可幫助潤髮及增加頭髮光澤。`;
  const [blogMessage, setBlogMessage] = useState(CryptoJS.AES.encrypt(text, hexkey, { mode: CryptoJS.mode.ECB }).toString());
  const [disable, setDisable] = useState(false);
  const keyValue = useRef('')

  function getHexKey(key) {
    return CryptoJS.enc.Hex.parse(key);
  }

  function decryptBlogMessage() {
    let blogPublicKey = getHexKey(keyValue.current.value);
    let plaintext = CryptoJS.AES.decrypt(blogMessage.toString(), blogPublicKey, { mode: CryptoJS.mode.ECB });
    setBlogMessage(plaintext.toString(CryptoJS.enc.Utf8));
    setDisable(true);
  }

  return (
    <div class="container-fluid">
      <div class="row">
        <h3>BLOG</h3>
        <p>
          <TextField id="keyValue" disabled={disable} defaultValue="000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f" size="small" helperText="" inputRef={keyValue} />
        </p>
        <p>
          <Button disabled={disable} onClick={()=> decryptBlogMessage()}>Decrypt</Button>
        </p>
        <p>{blogMessage}</p>
      </div>
    </div>
  )
}

export default DiarySectionPage

export const pageQuery = graphql`
  query MetadataQuery {
    site {
      siteMetadata {
        blogkey
      }
    }
  }
`