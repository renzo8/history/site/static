import { Link } from "gatsby";
import * as React from "react";

const KenncSectionPage = () => {
  return (
    <>
      <div class="container-fluid">
        <div class="row">
          <h3>KENNC . SECTIONS</h3>
          <div class="col">
            <ul>
              <li><Link to="/kennc/biz/">Business</Link></li>
              <li><Link to="/kennc/job/">Job</Link></li>
              <li><Link to="/kennc/living/yijin/lesson01/">易經</Link></li>
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}

export default KenncSectionPage
