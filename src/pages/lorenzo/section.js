import { Link } from "gatsby";
import * as React from "react";

const LorenzoSectionPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3>LORENZO . SECTIONS</h3>
        <h5>Cover Page</h5>
        <ul>
          <li>
            Education History
            <ul>
              <li>PN 2020-2021, Ming Wai International Preschool (Prince Edward Branch)</li>
              <li>K1 2021-2022, Sheng Kung Hui Kindergarten</li>
              <li>K2 2022-2023, Sheng Kung Hui Kindergarten</li>
              <li>K3 2023-2024, Sheng Kung Hui Kindergarten</li>
            </ul>
          </li>
          <li>
            Academic Achievement
            <ul>
              <li>English [ Cambridge ]</li>
              <li>Mandarin [ KPCC ]</li>
              <li>Mathematics [ IMO ]</li>
            </ul>
          </li>
          <li>
            Public Performance
            <ul>
              <li>2021 DEC 19, Christmas Performance (Lee Tung Avenue), Rainbow Melody Music Center</li>
              <li>2021 DEC 27, Christmas Performance (SKY Garden, Hysan Place), Rainbow Melody Music Center</li>
            </ul>
          </li>
          <li>
            Activity
            <ul>
              <li>Scout</li>
              <li>Music [ Choir, Piano ]</li>
            </ul>
          </li>
        </ul>
        <h5>Education</h5>
        <ul>
          <li><Link to="/lorenzo/education/history/">History</Link></li>
        </ul>
        <h5>Learning</h5>
        <ul>
          <li><Link to="/lorenzo/learning/language/english/">English</Link></li>
          <li><Link to="/lorenzo/learning/management/finance/">Finance</Link></li>
          <li><Link to="/lorenzo/learning/music/piano/">Piano</Link></li>
        </ul>
        <h5>Teaching</h5>
        <ul>
          <li><Link to="/lorenzo/teaching/methodology/">Methodology</Link></li>
        </ul>
      </div>
    </div>
  )
}

export default LorenzoSectionPage
