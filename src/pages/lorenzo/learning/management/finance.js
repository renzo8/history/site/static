import * as React from "react";
import Layout from "../../../components/common/visual/layout";

const FinancePage = () => {
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col">
          <h3>MANAGEMENT . FINANCE</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.richkid.com.tw/">i玩錢</a></li>
          </ul>
        </div>
      </div>
    </Layout>    
  )
}

export default FinancePage
