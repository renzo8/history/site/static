import * as React from "react";
import Layout from "../../../components/common/visual/layout";

const EnglishPage = () => {
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col">
          <h3>SCHOOLS . SKH</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.letterland.com/">Letterland</a></li>
          </ul>
          <h3>SCHOOLS. TUTOR</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.mingwai.edu.hk/">ABC Pathways (iStart, HKD$3200, 202x SEP/FEB)</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.mingwai.edu.hk/">British Council (幼兒常規英語課程 K1, HKD$ xxxx, anytime)</a></li>
          </ul>
          <h3>MATERIALS . LISTENING</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.bbc.co.uk/learningenglish/">BBC Learning English</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.thebritishschool.co.uk/phonics-1/">Mr. Thorne Does Phonics (suggested by UK schools)</a></li>
          </ul>
          <h3>MATERIALS . CARTOONS</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://my-best.tw/116034">Cartoons List 1</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.shemom.com/hot-topics/%E5%B0%8F%E6%9C%8B%E5%8F%8B%E5%AD%B8%E8%8B%B1%E6%96%87-netflix-%E9%81%A9%E5%90%88%E5%B0%8F%E5%AD%A9%E8%8B%B1%E6%96%87%E5%8D%A1%E9%80%9A%E6%8E%A8%E4%BB%8B/">Cartoons List 2</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.sundaykiss.com/%e5%b9%bc%e7%ab%a5/%e8%8b%b1%e6%96%87%e5%8d%a1%e9%80%9a-%e5%ad%b8%e8%8b%b1%e6%96%87-%e8%81%bd%e5%8a%9b%e6%95%99%e6%9d%90-cartoon-429418/15/">Cartoons List 3</a></li>
          </ul>
          <h3>MATERIALS . GAMES</h3>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.phonicsplay.co.uk/">PhonicsPlay</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.ictgames.com/">ictgames</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="http://www.readingeggs.co.uk/">ABC Reading Eggs</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://phonicsfamilycom.wordpress.com/useful-websites/">Phonics Family</a></li>
          </ul>
          <h3>OUTPUT AREA</h3>
          <ul>
            <li>World Family</li>
          </ul>
        </div>
      </div>
    </Layout>    
  )
}

export default EnglishPage
