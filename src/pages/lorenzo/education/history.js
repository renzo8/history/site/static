import * as React from "react";

const EducationPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3>KINDERGARTEN</h3>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.mingwai.edu.hk/">Ming Wai International Kindergarten</a></li>
        </ul>
        <h3>ENGLISH</h3>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.worldfamilyclub.com.hk/login.aspx">World Family</a></li>
          <li>ABC Pathways School</li>
        </ul>
        <h3>MUSIC</h3>
        <ul>
          <li><a target="_blank" rel="noopener noreferrer" href="https://www.singandyou.com/">Sing and You</a></li>
          <li><a target="_blank" rel="noopener noreferrer" href="http://rainbowmelodymusic.com/">Rainbow Melody</a></li>
          <li><a target="_blank" rel="noopener noreferrer" href="http://www.sammusic.asia/?lang=en">Silver Academy of Music (SAM)</a></li>
        </ul>
        <h3>SPORTS</h3>
        <ul>
          <li>Swimming</li>
        </ul>
        <h3>OTHERS</h3>
        <ul>
          <li>Chess</li>
        </ul>
        <h3>MUSIC THEORY</h3>
        <ul>
          <li>
              <b>奧福教學法</b>
              <p>循環 [ 音樂 → 律動 → 更具創意的律動 → 更具創意的音樂 ]</p>
              <p>香港奧福教育協會</p>
          </li>
          <li>高大宜教學法</li>
          <li>達克羅士/達克羅茲音樂節奏教學法</li>
          <li>埃德溫·戈登的音樂學習理論</li>
          <li>鈴木教學法</li>
        </ul>
      </div>
    </div>
  )
}

export default EducationPage
