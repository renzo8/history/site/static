import { faBook, faBookmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as React from "react";
import Layout from "../../components/common/visual/layout";

const MethodologyPage = () => {
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col">
          <h3>猶太教育</h3>
          <ul class="fa-ul">
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.ohpama.com/211741/%E8%A6%AA%E5%AD%90%E6%95%99%E9%A4%8A/%E8%A6%AA%E5%AD%90%E6%95%99%E9%A4%8A/%E7%8C%B6%E5%A4%AA%E7%88%B6%E6%AF%8D%E7%9A%84%E4%B9%9D%E9%81%93%E7%AE%A1%E6%95%99%E6%B3%95%E5%89%87-%E5%8A%A9%E5%AD%A9%E5%AD%90%E6%88%90%E7%82%BA%E4%B8%8B%E4%B8%80%E5%80%8B%E5%A4%A9%E6%89%8D/">猶太父母9道管教法則 助孩子成下一個天才</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://today.line.me/hk/v2/article/RWxv6e">猶太式教育9大方法</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://ppfocus.com/0/en75e1eb6.html">Havruta</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBook} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.books.com.tw/products/0010723177">哈柏露塔高效學習法</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBook} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.thenewslens.com/article/144719">猶太人成為全球頂尖人物的學習法</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBook} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.books.com.tw/products/0010679022">哈柏露塔，猶太人的教養祕訣：教出個性獨立，卻喜歡主動跟父母親近的孩子</a>
            </li>
          </ul>
          <h3>CHESS</h3>
          <ul class="fa-ul">
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.bookdepository.com/My-First-Chess-book-Katie-Daynes/9781474941082?redirected=true&utm_medium=Google&utm_campaign=Base1&utm_source=HK&utm_content=My-First-Chess-book&selectCurrency=HKD&w=AFF1AU96QU958QA8V9LP&gclid=EAIaIQobChMI8saD0MGg8wIVlqmWCh0kdAgpEAYYAiABEgLmyfD_BwE">My First Chess book</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.bookdepository.com/Usborne-Chess-Book-Lucy-Bowman/9781409598442?ref=pd_detail_1_sims_b_p2p_1">Usborne Chess Book</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.bookdepository.com/Usborne-Complete-Book-Chess-Elizabeth-Dalby/9781409574668?ref=pd_detail_1_sims_b_p2p_1">The Usborne Complete Book of Chess</a>
            </li>
            <li>
              <FontAwesomeIcon icon={faBookmark} size="1x" /><a class="icon-link" target="_blank" rel="noopener noreferrer" href="https://www.bookdepository.com/Maths-Games-for-Clever-Kids-R-Gareth-Moore/9781780555409?ref=pd_detail_1_sims_b_p2p_1">Maths Games for Clever Kids (R)</a>
            </li>
          </ul>
          <h3>MUSIC (basic)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
          <h3>SINGING (singing)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
          <h3>PRESENTATION (talking)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
          <h3>ENGLISH (language)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
          <h3>FOOTBALL (interest)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
          <h3>SWIMMING (interest)</h3>
          <ul class="fa-ul">
            <li>xxx</li>
          </ul>
        </div>
      </div>
    </Layout>    
  )
}

export default MethodologyPage
