import { Link } from "gatsby";
import * as React from "react";
import cookingFishReference from '../../data/pdf/cooking/science-cooking-fish.pdf';
import primarySchoolReference2019 from '../../data/pdf/education/lorenzo/top-500-state-primary-school-2019-times.pdf';
import secondarySchoolReference2019 from '../../data/pdf/education/lorenzo/top-500-state-secondary-school-2019-times.pdf';
import Layout from "../components/common/visual/layout";

const MoveSectionPage = () => {
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col page-title">MOVE . SECTIONS</div>
        <div class="col page-details">
          <h5>Emergency</h5>
          <p><a target="_blank" rel="noopener noreferrer" href="https://news.rthk.hk/rthk/ch/component/k2/1616963-20211026.htm">衞生署：接科興更改疫苗適用至3歲或以上, 交專家審批</a></p>
          <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <h5>Target</h5>
          <ul>
            <li><Link to="/move/area/barnet/">Barnet</Link></li>
            <li><Link to="/move/area/kingston/">Kingston</Link></li>
            <li><Link to="/move/area/reading/">Reading</Link></li>
            <li><Link to="/move/area/richmond/">Richmond</Link></li>
            <li><Link to="/move/area/sutton/">Sutton</Link></li>
          </ul>
          <h5>Custom</h5>
          <ul>
            <li><Link to="/move/custom/clearance/checklist/">Clearance Checklist</Link></li>
          </ul>
          <h5>Read</h5>
          <ul>
            <li><a href={primarySchoolReference2019} download>The top 500 English state primary schools [2019]</a></li>
            <li><a href={secondarySchoolReference2019} download>The top 500 English state secondary schools [2019]</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.bbc.com/zhongwen/trad/57360843">BBC: 一位單親母親攜女兒闖蕩英國的故事</a></li>
          </ul>
          <h5>Follow</h5>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://bymilly.co/">ByMilly</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://movetoeu.home.blog/">移民歐洲資訊站</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://lionrockplaza.com/2021/05/29/where-to-live-in-uk/">Lion Rock Plaza</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://ukgo.hk/author/admin/">英居</a></li>
          </ul>
          <h5>Todo</h5>
          <ul>
            <li><a target="_blank" rel="noopener noreferrer" href="https://good2share.app/%E7%A7%BB%E6%B0%91%E8%8B%B1%E5%9C%8B%E5%89%8D%E6%BA%96%E5%82%99%E4%BA%8B%E9%A0%85%E6%B8%85%E5%96%AE/">卡比</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.hkmoveuk.com/renting">過來人</a></li>
            <li><a target="_blank" rel="noopener noreferrer" href="https://www.landscope-international.com/package">Landscope</a></li>
            <li><a href={cookingFishReference} rel="noopener noreferrer" download>低溫烘烤魚 [120度, 9min]</a></li>
          </ul>
        </div>
      </div>
    </Layout>
  )
}

export default MoveSectionPage
