import { graphql } from "gatsby";
import * as React from "react";
import DataTable from 'react-data-table-component';
import '../../../../icon/fontawesome';
import Fsmodal from '../../../components/common/util/fsmodal';
import Layout from "../../../components/common/visual/layout";

const ListPage = ({ data }) => {
  const credentialColumns = [
    {
      name: 'ITEM',
      selector: 'item',
      sortable: true
    },
    {
      name: 'CATEGORY',
      selector: 'category',
      sortable: true
    },
    {
      name: 'OWNER',
      selector: 'owner',
      sortable: true
    },
    {
      name: 'RELATED-TO-IDENTITY',
      selector: 'identity_related',
      sortable: true
    },
    {
      name: 'RELATED-TO-HEALTH',
      selector: 'health_related',
      sortable: true
    },
    {
      name: 'RELATED-TO-JOB',
      selector: 'job_related',
      sortable: true
    },
    {
      name: 'REMARKS',
      selector: 'remarks',
      sortable: false
    }
  ];

  function getDataList(edges) {
    let tableData = [];
    edges.forEach(edge => {
      let row = {};
      row["item"] = edge.node.item;
      row["category"] = edge.node.category;
      row["owner"] = edge.node.owner;
      row["identity_related"] = edge.node.identity_related;
      row["health_related"] = edge.node.health_related;
      row["job_related"] = edge.node.job_related;
      row["remarks"] = edge.node.remarks;
      tableData.push(row);      
    });

    return tableData;
  }

  function getColumnList() {
    return credentialColumns;
  }
  
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col">
          <h3>CHECK LIST</h3>
          <p>
            {
              data.allFile.edges.map((file, index) => {
                if(file.node.name === "clearance") {
                  return (
                    <a href={file.node.publicURL} download>{file.node.name}</a>
                  )
                }
                return null;
              })
            }
          </p>
          <Fsmodal buttonLabel="Credential" modalTitle="Extracted data from excel check list" xlsFileName="clearance">
            <DataTable
                title={`Credential (` + data.allClearanceXlsxCredential.edges.length + `)`}
                columns={getColumnList()}
                data={getDataList(data.allClearanceXlsxCredential.edges)}
                theme="dark"
                selectableRows
            />
          </Fsmodal>
        </div>
      </div>
    </Layout>
  )
}

export default ListPage

export const query = graphql`
  query {
    allClearanceXlsxCredential {
      edges {
        node {
          item
          category
          owner
          identity_related
          health_related
          job_related
          remarks
        }
      }
    }
    allFile {
      edges {
        node {
          publicURL
          name
        }
      }
    }
  }
`