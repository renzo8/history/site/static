import * as React from "react";
import Layout from "../../components/common/visual/layout";

const RichmondPage = () => {
  return (
    <Layout pageTitle="">
      <div class="container-fluid">
        <div class="col">
          <h3>RICHMOND UPON THAMES</h3>
          <ul>
            <li>
              <a target="_blank" rel="noreferrer" href="https://en.wikipedia.org/wiki/London_Borough_of_Richmond_upon_Thames">wiki</a>
            </li>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.compare-school-performance.service.gov.uk/schools-by-type?step=default&table=schools&region=318&la-name=richmond-upon-thames&geographic=la&for=primary">
                compare schools
              </a>
            </li>
          </ul>
          <h5>Schools</h5>
          <ol>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.compare-school-performance.service.gov.uk/school/102908/holy-trinity-church-of-england-primary-school/primary">
                Holy Trinity Church of England Primary School
              </a>
              <ul>
                <li>
                  <a target="_blank" rel="noreferrer" href="https://www.locrating.com/school_catchment_areas.aspx?schoolid=urn101317&schooltype=0">
                    catchment
                  </a>
                </li>
                <li>
                  <a target="_blank" rel="noreferrer" href="https://www.rightmove.co.uk/property-to-rent/map.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22polylines%22%3A%22cxbyHhmy%40np%40yRqKabDmiAlJnd%40ljD%22%7D&numberOfPropertiesPerPage=499&sortType=2&propertyTypes=&viewType=MAP&mustHave=&dontShow=&furnishTypes=&viewport=-0.308251%2C-0.267052%2C51.4593%2C51.4711&keywords=">
                    rightmove
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.compare-school-performance.service.gov.uk/school/133728/marshgate-primary-school/primary">
                Marshgate Primary School
              </a>
            </li>
          </ol>
          <h5>Reference</h5>
          <ul>
            <li>XXX</li>
          </ul>
          <h5>Conflict</h5>
          <ul>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.compare-school-performance.service.gov.uk/schools-by-type?step=default&table=schools&region=318&la-name=richmond-upon-thames&geographic=la&for=primary">
                All Schools in Richmond
              </a>
            </li>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.londonpreprep.com/2020/07/londons-top-100-state-primary-schools-2019/">
                Comparison 1 [ londonpreprep ]
              </a>
            </li>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.dadi.com.hk/wp-content/uploads/2020/07/The-top-500-English-state-primary-schools-%E8%8B%B1%E5%9C%8B%E5%85%AC%E7%AB%8B%E5%B0%8F%E5%AD%B8TOP-500%E5%90%8D.pdf">
                Comparison 2 [ dadi ]
              </a>
            </li>
            <li>
              <a target="_blank" rel="noreferrer" href="https://www.mylondon.news/news/local-news/marshgate-primary-school-15655842">
                Comparison 3 [ mylondon ]
              </a>
            </li>
          </ul>
        </div>
      </div>
    </Layout>
  )
}

export default RichmondPage
