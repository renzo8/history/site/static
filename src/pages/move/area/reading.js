import * as React from "react";

const ReadingPage = () => {
  return (
    <div class="container-fluid">
      <div class="row">
        <h3>READING</h3>
        <p>
          <a target="_blank" rel="noreferrer" href="https://en.wikipedia.org/wiki/Reading,_Berkshire">wiki</a>
        </p>
      </div>
    </div>
  )
}

export default ReadingPage
