SET PORTABLE_GIT_HOME=C:\t3\e-imprest-upgrade\src\git\eias\deployment-script\meng\tools\git\PortableGit-2.32.0.2\cmd
SET NODEJS_HOME=C:\data-master\usr\kwkcheung\try\tools\nodejs\16.13.1

set PATH=%path%;%NODEJS_HOME%;%PORTABLE_GIT_HOME%;

git config --global user.name "kennc"
git config --global user.email "cheungwingkeung@protonmail.com"
git config --global http.proxy proxy1.scig.gov.hk:8080

npm config set strict-ssl false
npm config set proxy http://proxy1.scig.gov.hk:8080
npm config set https-proxy http://proxy1.scig.gov.hk:8080
npm config set registry https://registry.npmjs.org/ --global
rem npm config rm proxy
rem npm config rm https-proxy