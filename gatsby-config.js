module.exports = {
  pathPrefix: `/history/site/static`,
  siteMetadata: {
    url: `https://renzo8.gitlab.io/history/site/static/`,
    title: `Renzo`,
    author: `MengBow`,
    blogkey: `000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f`
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `csvData`,
        path: `${__dirname}/src/data/csv`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `jsonData`,
        path: `${__dirname}/src/data/json`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `xlsData`,
        path: `${__dirname}/src/data/xls`
      }
    },
    {
      resolve: `gatsby-transformer-excel`,
      options: {
        raw: false,
        defval: ""
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/src/data/diary`,
      },
    },
    `gatsby-transformer-csv`,
    `gatsby-plugin-react-helmet`
  ]
}
